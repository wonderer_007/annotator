require 'bundler/setup'
require 'em-twitter'

namespace :tweet_stream do

  desc "Stream tweets"
  task start: :environment do
    @tweets = []
    EM::run do

      options = {
        :path   => '/1.1/statuses/filter.json',
        :params => {
          :track            => HashTag.pluck(:name).join(",")
        },
        :oauth  => {
          :consumer_key     => ENV["consumer_key"],
          :consumer_secret  => ENV["consumer_secret"],
          :token            => ENV["token"],
          :token_secret     => ENV["token_secret"]
        }
      }

      client = EM::Twitter::Client.connect(options)
      puts "#{DateTime.now().strftime('%d/%m/%Y %I:%M:%S %P')} Starting Stream ...."
      client.each do |result|

        @test = result
        tweet = JSON.parse result
        location = location = tweet['user']['location']
        if location.nil?
           location = tweet['user']['time_zone']
           if !location.nil? && location.include?("Time") then location = time_zone_to_location(location) end
        end

        location = location.strip if !location.blank?
        if not location.nil?
          hash = { text: tweet['text'], url: tweet['url'], tweetable_type: 'HashTag', location: location, created: tweet['created_at'], tweet_id: tweet['id'] }
          @tweets << Tweet.new(hash)
          tweet_id = tweet['id']
          tweet_user = tweet['user']['screen_name'] if not tweet['user'].nil?
        end
      end

      client.on_error do |message|
        puts "oops: error: #{message}"
      end

      client.on_unauthorized do
        puts "oops: unauthorized"
      end

      client.on_forbidden do
        puts "oops: unauthorized"
      end

      client.on_not_found do
        puts "oops: not_found"
      end

      client.on_not_acceptable do
        puts "oops: not_acceptable"
      end

      client.on_too_long do
        puts "oops: too_long"
      end

      client.on_range_unacceptable do
        puts "oops: range_unacceptable"
      end

      client.on_enhance_your_calm do
        puts "oops: enhance_your_calm"
      end

      EM.add_timer(298) do
        EM.stop
        puts "#{DateTime.now().strftime('%d/%m/%Y %I:%M:%S %P')} Stream stop"
        puts "#{DateTime.now().strftime('%d/%m/%Y %I:%M:%S %P')} inserting #{@tweets.length} tweets"
        Tweet.import(@tweets)
        puts "#{DateTime.now().strftime('%d/%m/%Y %I:%M:%S %P')} Successfully inserted tweets in DB"
      end
    end
  end

  desc "Get Tag"
  task tag: :environment do

    hour = Time.now().hour
    api_key = ENV["mapzen#{hour%6}"]
    Geocoder.configure( timeout: 60, lookup: :mapzen, api_key: api_key, cache: Redis.new , cache_prefix: 'geocoder:')
    puts "Api Key is #{Geocoder.config.api_key}"

    puts "#{DateTime.now().strftime('%d/%m/%Y %I:%M:%S %P')} starting tagging tweets"
    hash_tags = HashTag.pluck(:name)
    hash = {}
    HashTag.pluck(:name, :id).each do |hash_tag|
      hash[hash_tag[0].downcase] = hash_tag[1] 
    end

    tweets = Tweet.select(:id, :tweetable_id, :text, :location, :city, :country, :lat, :long).where(tweetable_id: nil).limit(7000)
    json = {}
    count = 0

    tweets.each do |tweet|
      json[tweet.id] = {}
      tweet.text = tweet.text.downcase
      selected_hash_tags = hash_tags.select { |hash_tag| tweet.text.include?(hash_tag.downcase) }
      if !selected_hash_tags.blank?
        tweet.tweetable_id = hash[selected_hash_tags.first.downcase]
        json[tweet.id]['tweetable_id'] = hash[selected_hash_tags.first.downcase]
      else
        json[tweet.id]['tweetable_id'] = -1
      end

      location = tweet.location
      #puts "Searching for #{location}"
      result = Geocoder.search(location) if not location.nil?

      if result.blank? || result.first.cache_hit == false
        count +=1
        #puts "Mapzen has been hit #{count} times"
        if count%6 == 0
          #puts "Sleeping now"
          sleep 2
          #puts "Awake now"
        end
      elsif result.first.cache_hit == true
        #puts "Served from cache"
      end


        

      if not result.first.nil?
        json[tweet.id]['city'] = strip result.first.city
        json[tweet.id]['country'] = strip result.first.country
        json[tweet.id]['lat'] = result.first.coordinates[0]
        json[tweet.id]['long'] = result.first.coordinates[1]
      end
    end
    Tweet.update(json.keys, json.values)
    puts "#{DateTime.now().strftime('%d/%m/%Y %I:%M:%S %P')} Tweets Tagged"
    puts "Total mapzen hits #{count}"

  end

end

def time_zone_to_location time_zone
  location = time_zone.split("Time")[1]
  begin
    if not location.nil?
      location = location.sub("(", "")
      location = location.sub(")", "")
    end
  rescue Exception => e  
    puts e
  end
  location.strip
end

def strip string
  if not string.nil? then string = string.strip end
  string
end