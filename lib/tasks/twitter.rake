namespace :twitter do
  desc "Update trends"
  task trends: :environment do
    puts "#{DateTime.now().strftime('%d/%m/%Y %I:%M:%S %P')} Starting collecting trends"
    trends = CLIENT.trends
    trends.take(10).collect do |trend|
      if Trend.exists?(name: trend.name)
        obj = Trend.find_by(name: trend.name)
        obj.update(active: true, volume: trend.tweet_volume)
      else
        obj = Trend.create(name: trend.name, active: false, volume: trend.tweet_volume, max_id: 0)
      end
      get_tweets obj
      obj.active = true
      obj.save
    end
    puts "#{DateTime.now().strftime('%d/%m/%Y %I:%M:%S %P')} finished collecting trends"
  end

  def get_tweets hash_tag
    options = {count: ENV['tweets_per_request'], include_rts: false}
    options[:max_id] = hash_tag.max_id unless hash_tag.max_id.nil?
    result = CLIENT.search(hash_tag.name, options)
    obj = []
    result.take(ENV['get_per_tweet'].to_i).collect do |tweet|
      location = tweet.user.location

      if location.nil?
        location = tweet.user.time_zone
        if location.include?("Time") then location = time_zone_to_location(location) end
      end

      location = location.strip if !location.blank?

      if not location.nil?
        hash = {text: tweet.text, url: tweet.url, tweetable_id: hash_tag.id, tweetable_type: hash_tag.class.name, tweet_id: tweet.id, location: location, created: tweet.created_at }
        Geocoder.configure( timeout: 60, lookup: :mapzen, api_key: ENV['mapzen'], cache: Redis.new , cache_prefix: 'geocoder:')
        result = Geocoder.search(location) if not location.nil?
        if not result.first.nil?
          hash[:city] = strip result.first.city
          hash[:country] = strip result.first.country
          hash[:lat] = result.first.coordinates[0]
          hash[:long] = result.first.coordinates[1]
          obj << Tweet.new(hash)
        end
      end
    end

    Tweet.import obj
    puts "#{DateTime.now().strftime('%d/%m/%Y %I:%M:%S %P')}  Collected #{obj.count} tweets for #{hash_tag.name}"
    hash_tag.max_id = hash_tag.tweets.order("tweet_id ASC").first.tweet_id if hash_tag.tweets.count > 1
    hash_tag.save
    #system("python2.7 /home/deploy/twitter-sentiment-analyzer/mytest.py")

    sleep 2
  end

  def time_zone_to_location time_zone
    location = time_zone.split("Time")[1]
    begin
      if not location.nil?
        location = location.sub("(", "")
        location = location.sub(")", "")
      end
    rescue Exception => e  
      puts e
    end
    location.strip
  end

  def strip string
    if not string.nil? then string = string.strip end
    string
  end

end



