class RemoveAndAddIndex < ActiveRecord::Migration
  def change
    remove_index :tweets, :tweet_id
    add_index :tweets, :trend_id
  end
end
