class AddCountryCityInTweets < ActiveRecord::Migration
  def change
    add_column :tweets, :country, :string
    add_column :tweets, :city, :string

    add_index :tweets, [:trend_id, :country]
  end
end
