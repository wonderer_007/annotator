class CreateMyTweets < ActiveRecord::Migration
  def change
    create_table :my_tweets do |t|
      t.text :json

      t.timestamps null: false
    end
  end
end