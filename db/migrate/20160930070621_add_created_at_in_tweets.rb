class AddCreatedAtInTweets < ActiveRecord::Migration
  def change
    add_column :tweets, :created, :string
  end
end
