class AddTimeZoneInTweet < ActiveRecord::Migration
  def change
    add_column :tweets, :time_zone, :string
    add_column :tweets, :lat, :string
    add_column :tweets, :long, :string
  end
end
