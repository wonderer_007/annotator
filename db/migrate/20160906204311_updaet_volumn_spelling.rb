class UpdaetVolumnSpelling < ActiveRecord::Migration
  def change
    remove_column :trends, :volumn
    add_column :trends, :volume, :boolean, :default => false
  end
end