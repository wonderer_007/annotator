class UdateVolumeType < ActiveRecord::Migration
  def change
    remove_column :trends, :volume
    add_column :trends, :volume, :integer, :default => 0
  end
end