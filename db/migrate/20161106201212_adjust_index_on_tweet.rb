class AdjustIndexOnTweet < ActiveRecord::Migration

  def change
    remove_index :tweets, :trend_id
    remove_index :tweets, [:trend_id, :country]
    add_index :tweets, [:tweetable_id, :tweetable_type]
    add_index :tweets, [:tweetable_id, :tweetable_type, :country]
  end

end