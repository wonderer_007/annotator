class AddMaxIdInTrend < ActiveRecord::Migration
  def change
    add_column :trends, :max_id, :string
  end
end