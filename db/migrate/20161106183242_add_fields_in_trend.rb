class AddFieldsInTrend < ActiveRecord::Migration
  def change
    add_column :tweets, :tweetable_id, :integer
    add_column :tweets, :tweetable_type, :string, :default => "Trend"
  end
end