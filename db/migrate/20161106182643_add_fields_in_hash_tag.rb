class AddFieldsInHashTag < ActiveRecord::Migration
  def change
    add_column :hash_tags, :name, :string
    add_column :hash_tags, :active, :boolean, :default => false
    add_column :hash_tags, :volume, :integer, :default => 0
    add_column :hash_tags, :max_id, :string
  end
end