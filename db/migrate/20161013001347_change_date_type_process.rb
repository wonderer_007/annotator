class ChangeDateTypeProcess < ActiveRecord::Migration
  def change
	change_column :tweets, :process, :integer, :default => 0
  end
end
