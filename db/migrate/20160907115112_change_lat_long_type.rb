class ChangeLatLongType < ActiveRecord::Migration
  def change
    remove_column :tweets, :lat
    remove_column :tweets, :long
    add_column :tweets, :lat, :float
    add_column :tweets, :long, :float
  end
end
