class AddProcessInTweets < ActiveRecord::Migration
  def change
	add_column :tweets, :process, :boolean, :default => false
  end
end
