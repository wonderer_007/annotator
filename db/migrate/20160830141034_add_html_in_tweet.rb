class AddHtmlInTweet < ActiveRecord::Migration
  def change
    add_column :tweets, :html, :string
  end
end
