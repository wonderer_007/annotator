class AddHandleIdInTweet < ActiveRecord::Migration
  def change
    add_column :tweets, :handle_id, :integer
  end
end
