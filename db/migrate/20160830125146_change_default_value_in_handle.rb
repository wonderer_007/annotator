class ChangeDefaultValueInHandle < ActiveRecord::Migration
  def change
    change_column :handles, :flag, :boolean, :default => false
  end
end
