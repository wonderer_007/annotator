class CreateTrends < ActiveRecord::Migration
  def change
    create_table :trends do |t|
      t.string :name
      t.boolean :active
      t.integer :volumn

      t.timestamps null: false
    end
  end
end
