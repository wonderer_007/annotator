ActiveAdmin.register Tweet do

  filter :handle
  filter :tweet_id
  filter :handle_id
  filter :time_zone

  index do
    selectable_column
    id_column
    column :id
    column :tweet_id
    column :url
    column :trend
    column :time_zone
    column :lat
    column :long
    column :sentiment

    actions
  end

end