ActiveAdmin.register HashTag do

  before_filter :skip_sidebar!
  permit_params :name, :max_id, :volume, :active


  index do
    selectable_column
    id_column
    column :id
    column :name
    column :max_id
    column :volume
    column :active

    actions
  end

end