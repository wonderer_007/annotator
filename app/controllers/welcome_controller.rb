class WelcomeController < ApplicationController

  before_action :set_tweet, only: [:sentiment]

  def home
  end

  def annotation
    @tweet = Tweet.where(flag: false).sample
    if !@tweet.nil?
      @url = @tweet.url
    end
  end

  def map
    @trend = Trend.current.find params[:id]
    @tweets = @trend.tweets.limit(200)
    @hash = Gmaps4rails.build_markers(@tweets) do |tweet, marker|
      marker.lat tweet.lat
      marker.lng tweet.long
    end

  end

  def sentiment

    if !params[:sentiment].empty? and [-1,1,0,2].include? params[:sentiment].to_i
      @tweet.sentiment = params[:sentiment]
      @tweet.flag = true
      flash[:notice] = "Sentiment lebel applied successfully"
      @tweet.save 
   end

    redirect_to annotation_path

  end

  def trends
    @trends = Trend.current
    @data = {}
    @data[:trends] = []
    @trends.each do |trend|
      @data[:trends] << {name: trend.name, id: trend.id, last_updated_time: trend.updated_at.to_i,
                          start_time: trend.created_at.to_i, 
                          positive: trend.tweets.where(sentiment:0).count,
                          negative: trend.tweets.where(sentiment:-1).count,
                          neutral: trend.tweets.where(sentiment:0).count
                        }
    end

    render json: @data
  end

  def hashtags
    @trends = HashTag.current
    @data = {}
    @data[:trends] = []
    @trends.each do |trend|
      @data[:trends] << {name: trend.name, id: trend.id, last_updated_time: trend.updated_at.to_i,
                          start_time: trend.created_at.to_i,
                          positive: trend.tweets.where(sentiment:0).count,  
                          negative: trend.tweets.where(sentiment:-1).count,
                          neutral: trend.tweets.where(sentiment:0).count
                        }
    end

    render json: @data
  end


  def stats
    @trend = Trend.find params[:id]
    data = {}

    if not @trend.nil?
      data[:sentiment] = {}
      data[:status] = 200

      countries = Tweet.where(tweetable_id: @trend.id).uniq.pluck(:country)

      countries.map do |country|
        if country.nil?
          key = "others"
        else
          key = country
        end
        data[:sentiment][key] = Tweet.where(tweetable_id: @trend.id, country:country).group(:sentiment).count
      end

    else
      data[:status] = 404
      data[:msg] = "Trend with id #{params[:id]} not found"
    end

    render json: data
  end

  def tweets_api
    @trend = Trend.find params[:id]
    data = {}

    if not @trend.nil?
      data[:sentiment] = {}
      data[:status] = 200
      data[:sentiment][:positive] = @trend.tweets.where(sentiment: 1).count
      data[:sentiment][:negative] = @trend.tweets.where(sentiment: -1).count
      data[:sentiment][:neutral] = @trend.tweets.where(sentiment: 0).count

      data[:tweets] = @trend.tweets.page params[:page]
    else
      data[:status] = 404
      data[:msg] = "Trend or HashTag with id #{params[:id]} not found"
    end

    render json: data
  end

  def hash_tag_tweets
    @hash_tag = HashTag.find params[:id]
    data = {}

    if not @hash_tag.nil?
      data[:sentiment] = {}
      data[:status] = 200
      data[:sentiment][:positive] = @hash_tag.tweets.where(sentiment: 1).count
      data[:sentiment][:negative] = @hash_tag.tweets.where(sentiment: -1).count
      data[:sentiment][:neutral] = @hash_tag.tweets.where(sentiment: 0).count

      data[:tweets] = @hash_tag.tweets.page params[:page]
    else
      data[:status] = 404
      data[:msg] = "Trend with id #{params[:id]} not found"
    end

    render json: data
  end

  def hash_tag_stats
    @hash_tag = HashTag.find params[:id]
    data = {}

    if not @hash_tag.nil?
      data[:sentiment] = {}
      data[:status] = 200

      countries = Tweet.where(tweetable_id: @hash_tag.id).uniq.pluck(:country)

      countries.map do |country|
        if country.nil?
          key = "others"
        else
          key = country
        end
        data[:sentiment][key] = Tweet.where(tweetable_id:@hash_tag.id, country:country).group(:sentiment).count
      end

    else
      data[:status] = 404
      data[:msg] = "HashTag with id #{params[:id]} not found"
    end

    render json: data
  end

  def themes

    @hash_tag = HashTag.find params[:id]
    countries = Tweet.where(tweetable_id: @hash_tag.id).uniq.pluck(:country)
    @countries = []

    countries.each_with_index do |country, index|
      @countries[index] = {}
      @countries[index]['name'] = country
      @countries[index]['volume'] = @hash_tag.tweets.where(country:country).count
      @countries[index]['positive'] = @hash_tag.tweets.where(country:country, sentiment: 1).count
      @countries[index]['negative'] = @hash_tag.tweets.where(country:country, sentiment: -1).count
      @countries[index]['neutral'] = @hash_tag.tweets.where(country:country, sentiment: 0).count
    end

  end

  def all_themes
    @hash_tags = HashTag.all
  end

  private
    def set_tweet
      if Tweet.exists?(id: params[:tweet])
        @tweet = Tweet.find params[:tweet]
      else
        redirect_to root_path
      end
    end

end
