class DataController < ApplicationController

  def form
  end

  def countries
    hash_tag = HashTag.find params[:id]
    countries = hash_tag.tweets.order('country asc').pluck(:country).uniq
    render :json => countries
  end

  def datasheet
    @hash_tag = HashTag.find params[:hash_tag]
    @country = params[:country]
    @from = params[:start].to_time
    @to = params[:end].to_time
    @result = @hash_tag.tweets.where("country = ? AND UNIX_TIMESTAMP(STR_TO_DATE(`created`, '%a %b %d %H:%i:%s +0000 %Y')) >= ? AND UNIX_TIMESTAMP(STR_TO_DATE(`created`, '%a %b %d %H:%i:%s +0000 %Y')) < ?", @country, @from.to_i, @to.to_i).group("CAST(STR_TO_DATE(`created`, '%a %b %d %H:%i:%s +0000 %Y') AS DATE)").order("CAST(STR_TO_DATE(`created`, '%a %b %d %H:%i:%s +0000 %Y') AS DATE) asc").count
  end

  def tweets
    @hash_tag = HashTag.find params[:id]
    @time = params[:time]
    @country = params[:country]
    @tweets = @hash_tag.tweets.where("country = ? AND UNIX_TIMESTAMP(STR_TO_DATE(`created`, '%a %b %d %H:%i:%s +0000 %Y')) = ?", @country, @time).order("CAST(STR_TO_DATE(`created`, '%a %b %d %H:%i:%s +0000 %Y') AS DATE) asc")
  end

  def stats
    @data =  HashTag.find_by_sql("SELECT hash_tags.name, COUNT(*) as count FROM hash_tags JOIN tweets ON hash_tags.id = tweets.tweetable_id WHERE tweets.tweetable_type='HashTag' GROUP BY hash_tags.name ORDER BY hash_tags.id")
  end

end