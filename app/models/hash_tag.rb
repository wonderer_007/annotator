class HashTag < ActiveRecord::Base

  has_many :tweets, :as => :tweetable

  def self.current
    HashTag.where(active:true)
  end

  def self.options_for_select
    HashTag.select(:id, :name).collect {|hash_tag| [hash_tag.name, hash_tag.id]}
  end

end