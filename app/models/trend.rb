class Trend < ActiveRecord::Base

  has_many :tweets, :as => :tweetable

  def self.current
    trends = Trend.where(active:true).order('updated_at desc').limit(20)
    final_trends = []
    trends.map do |t|
      final_trends << t if t.tweets.count > 50
    end
    final_trends
  end

  def positive
    self.tweets.where(sentiment:1).count
  end

  def negative
    self.tweets.where(sentiment:-1).count
  end

  def neutral
    self.tweets.where(sentiment:0).count
  end

  def self.last_updated_at
    !Trend.where(active: true).blank? ? Trend.where(active: true).first.updated_at : ''
  end

end
