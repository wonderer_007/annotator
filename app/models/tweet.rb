class Tweet < ActiveRecord::Base
	paginates_per 500
  belongs_to :tweetable, :polymorphic => true
end