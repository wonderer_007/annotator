Rails.application.routes.draw do

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  root 'welcome#home'
  get '/annotation', to: 'welcome#annotation', :as => :annotation
  get '/map/:id', to: 'welcome#map', :as => :map
  post '/sentiment', to: 'welcome#sentiment'

  get '/api/trends', to: 'welcome#trends'
  get '/api/trends/:id', to: 'welcome#stats'
  get '/api/tweets/:id', to: 'welcome#tweets_api'

  get '/api/hashtags', to: 'welcome#hashtags'
  get '/api/hash_tag_trends/:id', to: 'welcome#hash_tag_stats'
  get '/api/hash_tag_tweets/:id', to: 'welcome#hash_tag_tweets'

  get '/all_themes/', to: 'welcome#all_themes' 
  get '/themes/:id', to: 'welcome#themes' 

  get '/form', to: 'data#form'
  get '/countries/:id', to: 'data#countries'
  post '/datasheet', to:'data#datasheet', :as => :data_sheet
  get '/tweets/:time/:country/:id', to: 'data#tweets'
  get '/stats', to: 'data#stats'

end