set :output, "log/cron_log.log"
set :environment, "development"

every 20.minutes do
  rake "twitter:trends"
end

every 5.minutes do
  rake "tweet_stream:start"
end

every 10.minutes do
  rake "tweet_stream:tag"
end